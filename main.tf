terraform {
  required_version = ">= 0.11.3" # When locals were introduced
}

locals {
  region = "${var.append_region_to_name == "true" ? var.region : "" }"
  name   = "terraform-${join("-", distinct(compact(list(var.project_name, var.environment, local.region))))}-tfstate"
}


resource "aws_dynamodb_table" "default" {
  name           = "${local.name}-lock"
  read_capacity  = "${var.read_capacity}"
  write_capacity = "${var.write_capacity}"
  hash_key       = "LockID"

  server_side_encryption {
    enabled = true
  }

  attribute {
    name = "LockID"
    type = "S"
  }

  tags = "${var.tags}"
}
