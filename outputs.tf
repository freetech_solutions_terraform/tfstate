output "dynamodb_table_name" {
  value       = "${aws_dynamodb_table.default.name}"
  description = "DynamoDB table name"
}

output "dynamodb_table_id" {
  value       = "${aws_dynamodb_table.default.id}"
  description = "DynamoDB table ID"
}

output "dynamodb_table_arn" {
  value       = "${aws_dynamodb_table.default.arn}"
  description = "DynamoDB table ARN"
}
